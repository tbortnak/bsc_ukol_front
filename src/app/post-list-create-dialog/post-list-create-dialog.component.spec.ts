import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostListCreateDialogComponent } from './post-list-create-dialog.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {createTranslateLoader} from "../app.module";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef, MatInputModule} from "@angular/material";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {FormsModule} from "@angular/forms";

describe('PostListCreateDialogComponent', () => {
  let component: PostListCreateDialogComponent;
  let fixture: ComponentFixture<PostListCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostListCreateDialogComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports : [MatDialogModule, HttpClientModule, FormsModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      })],

      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
