import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Post} from "../model/post";
import {PostApiService} from "../services/post-api.service";

@Component({
  selector: 'app-post-list-create-dialog',
  templateUrl: './post-list-create-dialog.component.html',
  styleUrls: ['./post-list-create-dialog.component.less']
})
export class PostListCreateDialogComponent implements OnInit {

  constructor(
      public dialogRef: MatDialogRef<PostListCreateDialogComponent>,
      private postApiService: PostApiService,
      @Inject(MAT_DIALOG_DATA) public datas = new Post()) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onCreateClick(): void {
    console.log(this.datas);
    this.postApiService.createPost(this.datas).subscribe(value => {
      console.log("successfully created")
    }, error1 => {
      console.log(error1);
      this.dialogRef.close();
    }, () => {
      console.log("request finished");
      this.dialogRef.close();
    });

  }

  ngOnInit(): void {
  }

}
