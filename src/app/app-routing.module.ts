import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from "./main-page/main-page.component";
import {PageNotFoundComponentComponent} from "./page-not-found-component/page-not-found-component.component";
import {PostListComponent} from "./post-list/post-list.component";
import {TodoListComponent} from "./todo-list/todo-list.component";


const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'post', component: PostListComponent},
  {path: 'todo', component: TodoListComponent},

  {path: '**', component: PageNotFoundComponentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
