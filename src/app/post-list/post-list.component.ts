import {Component, OnInit} from '@angular/core';
import {PostApiService} from "../services/post-api.service";
import {Post} from "../model/post";
import {MatDialog} from "@angular/material";
import {PostListCreateDialogComponent} from "../post-list-create-dialog/post-list-create-dialog.component";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.less']
})
export class PostListComponent implements OnInit {

  private postList: Post[];

  constructor(private postApiService: PostApiService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    console.log("on init");
    this.postApiService.getAllPosts().subscribe((response: Post[]) => {

        this.postList = response;
        console.log(this.postList);
      },
      error1 => {
        console.error(error1)
      },
      () => {
        console.log("complete")
      });


  }

  onDeleteClick(id): void {
    this.postApiService.deletePost(id).subscribe(value => {
      console.log("successfully deleted")
    }, error1 => {
      console.log(error1);
  });

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PostListCreateDialogComponent, {
      width: '250px'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
