import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoDetailDialogComponent } from './todo-detail-dialog.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CUSTOM_ELEMENTS_SCHEMA, forwardRef} from "@angular/core";
import {FormsModule, NG_VALUE_ACCESSOR} from "@angular/forms";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {createTranslateLoader} from "../app.module";

describe('TodoDetailDialogComponent', () => {
  let component: TodoDetailDialogComponent;
  let fixture: ComponentFixture<TodoDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoDetailDialogComponent ],
      imports: [MatDialogModule, HttpClientModule, FormsModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      })],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],


      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} }
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
