import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Todo} from "../model/todo";
import {TodoApiService} from "../services/todo-api.service";

@Component({
  selector: 'app-todo-detail-dialog',
  templateUrl: './todo-detail-dialog.component.html',
  styleUrls: ['./todo-detail-dialog.component.less']
})
export class TodoDetailDialogComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<TodoDetailDialogComponent>,
    private todoApiService: TodoApiService,
    @Inject(MAT_DIALOG_DATA) public data: Todo) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    console.log(this.data);
    this.todoApiService.updateTodo(this.data).subscribe(value => {
      console.log("successfully updated")
    }, error1 => {
      console.log(error1);
      this.dialogRef.close();
    }, () => {
      console.log("request finished");
      this.dialogRef.close();
    });

  }

  ngOnInit(): void {
  }


}
