import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../model/post";
import {Observable} from "rxjs/internal/Observable";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PostApiService {

  url = 'http://jsonplaceholder.typicode.com';

  constructor(private httpClient: HttpClient) {
  }

  public getAllPosts(): Observable<any> {
    console.log(this.url);

    return this.httpClient.get(this.url + '/posts');

  }

  public deletePost (id: number): Observable<{}> {
    return this.httpClient.delete(this.url + '/posts/' + id);
  }

  public createPost(post : Post): Observable<{}>{
    return this.httpClient.post<Post>(this.url + '/posts', post)
  }

/*  public adNewPost() */
}

