import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../model/post";
import {Observable} from "rxjs/internal/Observable";
import {Todo} from "../model/todo";

@Injectable({
  providedIn: 'root'
})
export class TodoApiService {

  url = 'http://jsonplaceholder.typicode.com';

  constructor(private httpClient: HttpClient) {
  }

  public getAllTodos(): Observable<any> {
    return this.httpClient.get(this.url + '/todos');
  }

  public updateTodo(todo:Todo): Observable<any> {
    return this.httpClient.put(this.url + '/todos/' + todo.id, todo);
  }
}
