import { Component, OnInit } from '@angular/core';
import {TodoApiService} from "../services/todo-api.service";
import {Todo} from "../model/todo";
import {MatDialog} from "@angular/material";
import {TodoDetailDialogComponent} from "../todo-detail-dialog/todo-detail-dialog.component";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.less']
})
export class TodoListComponent implements OnInit {

  private todoList: Todo[];

  constructor(private todoApiService: TodoApiService,
              public dialog: MatDialog) { }

  ngOnInit() {
    console.log("on init");
    this.todoApiService.getAllTodos().subscribe((response: Todo[]) => {

        this.todoList = response;
        console.log(this.todoList);
      },
      error1 => {
        console.error(error1)
      },
      () => {
        console.log("complete")
      });


  }


  openDialog(todo:Todo): void {
    const dialogRef = this.dialog.open(TodoDetailDialogComponent, {
      width: '250px',
      data: todo

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
