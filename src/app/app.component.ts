import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'bsc-front';

  constructor(public translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('cs');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');
  }

  toEN(): void {
    this.translate.use('en');
  }

  toCZ(): void {
    this.translate.use('cs');
  }
}
